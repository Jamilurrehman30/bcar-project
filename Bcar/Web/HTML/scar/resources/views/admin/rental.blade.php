@extends('admin.layout.base')

@section('title', 'Car Rental')

@section('content')
  
   
    <form>
   <div class="form-group row">
                    <label for="fixed" class="col-xs-2 col-form-label">Cancellation Fee</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="" name="fixed" required id="fixed" placeholder="Price">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="distance" class="col-xs-2 col-form-label">Waiting Fee</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="" name="distance" required id="distance" placeholder="Car Type">
                    </div>
                </div>

                </div>

                <div class="form-group row">
                    <label for="price" class="col-xs-2 col-form-label">Total Distance</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="" name="price" required id="price" placeholder="Total Distance">
                    </div>
                </div>

 

        
              
                    <div class="form-group row">
                        <label for="zipcode" class="col-xs-2 col-form-label"></label>
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update Record</button>
                            <a href="" class="btn btn-default">Cancel</a>
                        </div>
                    </div></form>
  

@endsection
