<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        
        <title>Norile</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/animations.css">
        <link rel="stylesheet" href="css/font-awesome.min.css"> 
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <!--[if lt IE 9]>
            <script src="js/vendor/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


<div class="head" style="background-color: white">
		<div class="container">
        <div class="row">
            <div id="head1">
                <div class="col-md-3">           
                         <div class="logo" >
                            <a href="">
                                <div class="img-logo">
                                    <img alt="" src="example/logo.png" width="240px" height="65px">
                                </div>
                            </a>
                        </div>            
                </div>

                <div class="col-md-4 middle" style="padding-bottom: 10px" >                     
                                         
                </div>
                <div class="col-md-3 socials" style="padding-top: 0px">
                 <a class="theme_btn" href="{{ url('/provider/register') }}" style="border-radius: 18px">Become a Driver</a>

               </div> 
             
                 <div class="col-md-2 socials" style="padding-top: 0px">
                 <a class="theme_btn" href="{{ url('rideweb')}}" style="border-radius: 18px">Take a Ride</a>
                 
               </div> 
                
            </div> 
        </div> 
    </div> 
</div>


<section id="mainslider" class="">

        <div class="flexslider">
           <ul class="slides">
               <li>
                    <video style="opacity: 0.5%" autoplay muted loop id="myVideo" width="100%" height="90%"> <source src="example/tra.mp4" type="video/mp4"></video>
                    <div class="slide_description_wrapper">
                        <div class="slide_description">
                            <h3  data-animation="slideUp">Taxi Service</h3>
                            <h2 data-animation="slideUp"></h2>                            
                            <p data-animation="slideUp">                               
                                <a class="theme_btn" href="{{ url('rideweb')}}">Sign Up</a>
                            </p>                            
                        </div>
                    </div> 
                </li>
                <li>
                	<video style="opacity: 0.5%" autoplay muted loop id="myVideo" width="100%" height="90%"> <source src="example/tra.mp4" type="video/mp4"></video>
                    <div class="slide_description_wrapper">
                        <div class="slide_description">
                            <h3 data-animation="slideUp">Delivery Services </h3>
                            <h2 data-animation="slideUp"></h2>                            
                            <p data-animation="slideUp">                               
                                <a class="theme_btn" href="{{ url('rideweb')}}">Sign Up</a>
                            </p>  
                        </div> 
                    </div> 
                </li>   
                 <li>
                	<video style="opacity: 0.5%" autoplay muted loop id="myVideo" width="100%" height="90%"> <source src="example/tra.mp4" type="video/mp4"></video>
                    <div class="slide_description_wrapper">
                        <div class="slide_description">
                            <h3 data-animation="slideUp">Rent a Car Services </h3>
                            <h2 data-animation="slideUp"></h2>                            
                            <p data-animation="slideUp">                               
                                <a class="theme_btn" href="{{ url('rideweb')}}">Sign Up</a>
                            </p>  
                        </div> 
                    </div> 
                </li>              
            </ul>
        </div>
  </section>

 <header id="header" class="light_section">
        <div class="container">
            <div class="row">                
                <div class="col-sm-12 mainmenu_wrap">
                    <div class="main-menu-icon visible-xs">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <nav>
                        <ul id="mainmenu" class="nav menu sf-menu responsive-menu superfish">
                            <li class="">
                                <a href="#top">Home</a>
                            </li>
                            <li class="">
                                <a href="#about" id="aboutbtn">About Us</a>
                            </li>
                            <li class="">
                                <a href="#team" id="joinbtn">Join</a>
                             </li>  
                             <li class="">
                                <a href="#service" id="servicebtn">Service</a>
                             </li>  
                                      
                            <li class="">
                               <a href="#contact" id="contactbtn">Contact</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>


<section id="about" class="">
  <div class="container">    
    <div class="row ">
      <div class="col-sm-6 col-sm-offset-3">   
	     <h3 class="head-title text-center to_animate" data-animation="slideDown">About Us</h3>  	      
	      <p class="head-desc text-center to_animate" data-animation="fadeInUp">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          <hr class="title-border">
      </div>
	</div>

	<div class="row">        
    <div class="col-md-3 col-sm-6 title-item">     
        <div class="rt-icon-pen2 icon_title">          
        </div>
          <div class="title-content">
              <h4 class="title">Our plans</h4>      
               <p class="title_description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus adipiscing eget augue at tristique. Aliquam sit amet sodales dui, eget blandit metus. Nam fermentum purus eu est cursus tincidunt. Proin ut justo lorem. </p>          
             
          </div>
    </div> 
    
     <div class="col-md-3 col-sm-6 title-item">     
        <div class="rt-icon-world icon_title">          
        </div> 
           <div class="title-content">
              <h4 class="title">Education</h4>      
               <p class="title_description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus adipiscing eget augue at tristique. Aliquam sit amet sodales dui, eget blandit metus. Nam fermentum purus eu est cursus tincidunt. Proin ut justo lorem. </p>       
              
            </div>
    </div>     
     <div class="col-md-3 col-sm-6 title-item">  
        <div class="rt-icon-gift icon_title">          
        </div> 
            <div class="title-content">
              <h4 class="title">Help</h4>      
               <p class="title_description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus adipiscing eget augue at tristique. Aliquam sit amet sodales dui, eget blandit metus. Nam fermentum purus eu est cursus tincidunt. Proin ut justo lorem. </p>        
             
           </div>
    </div> 
        <div class="col-md-3 col-sm-6 title-item">  
        <div class="rt-icon-briefcase icon_title">          
        </div> 
            <div class="title-content">
              <h4 class="title">Support</h4>      
               <p class="title_description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus adipiscing eget augue at tristique. Aliquam sit amet sodales dui, eget blandit metus. Nam fermentum purus eu est cursus tincidunt. Proin ut justo lorem. </p>        
             
           </div>
    </div>
    </div>   
  </div>
</section>


   
<section class="welcome-box">
    <div class="container" >
      <div class="row ">
            <div class="col-sm-6 col-sm-offset-3">   
             <h3 class="head-title text-center to_animate" data-animation="slideDown">Welcome to Norile</h3>
              <p class="head-desc text-center to_animate" data-animation="fadeInUp">Smartest Taxi and Delivery Service</p>
                <hr class="title-border">
            </div>
        </div>
        <div class="row">            
            <div class="col-sm-7 to_animate" data-animation="fadeInLeft"> 
             <div class="slide_description_wrapper">               
                  <h3>Tap the app, get a ride</h3>
                  <p>Norile is the smartest way to get around. One tap and a car comes directly to you. Your driver knows exactly where to go. And you can pay with either cash or card..</p>    
               
              </div>    
            </div>
            <div class="col-sm-5 to_animate" data-animation="fadeInRight">
                <div class="slide_image_wrapper">
                    <img src="example/welcom_slide.jpg" alt="">
                </div>                              
            </div>
        </div>
    </div>
</section>  
<section class="welcome-box">
    <div class="container" >
    
        <div class="row"> 
        <div class="col-sm-5 to_animate" data-animation="fadeInRight">
                <div class="slide_image_wrapper">
                    <img src="example/anywhere.png" alt="">
                </div>                              
            </div>    
            <div class="col-sm-1 to_animate" data-animation="fadeInRight">
                                            
            </div>       
            <div class="col-sm-6 to_animate" data-animation="fadeInLeft"> 
             <div class="slide_description_wrapper">               
                  <h3>Ready anywhere, anytime</h3>
                  <p>Daily commute. Errand across town. Early morning flight. Late night drinks. Wherever you’re headed, count on Norile for a ride—no reservations needed...</p>    
               
              </div>    
            </div>
            
        </div>
    </div>
</section> 
<section class="welcome-box">
    <div class="container" >
  
        <div class="row">            
            <div class="col-sm-7 to_animate" data-animation="fadeInLeft"> 
             <div class="slide_description_wrapper">               
                  <h3>Low-cost to luxury</h3>
                  <p>You can always request everyday cars at everyday prices. But sometimes you need a bit more space. Or you want to go big on style. With Norile, the choice is yours..</p>   
           </div>
            </div>
            <div class="col-sm-5 to_animate" data-animation="fadeInRight">
                <div class="slide_image_wrapper">
                    <img src="example/low-cost.png" alt="">
                </div>                              
            </div>
        </div>
    </div>
</section>  
<section class="welcome-box">
    <div class="container" >
    
        <div class="row"> 
        <div class="col-sm-4 to_animate" data-animation="fadeInRight">
                <div class="slide_image_wrapper">
                    <img src="example/DeliveryMan.png" alt="">
                </div>                              
            </div>    
            <div class="col-sm-1 to_animate" data-animation="fadeInRight">
                                            
            </div>       
            <div class="col-sm-7 to_animate" data-animation="fadeInLeft"> 
             <div class="slide_description_wrapper">               
                  <h3>Delivering Anywhere Anytime</h3>
                  <p>Norile is the fastest way to deliver your packages with minimal cost </p>
              </div>    
            </div>
            
        </div>
    </div>
</section> 

<section id="offer" class="">
      <div class="container">
          <div class="row ">
            <div class="col-sm-6 col-sm-offset-3">   
             <h3 class="head-title text-center to_animate" data-animation="slideDown">What We Offer</h3>
              <p class="head-desc text-center to_animate" data-animation="fadeInUp">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                <hr class="title-border">
            </div>
         </div>
          
       <div class="row">
          <article class="col-md-3 col-sm-6 col-xs-6">
              <div  class="hexagon figure">
                  <div style="background-color: white" class="full-hexagon">
                      <div class="hex-upper hex-box"></div>
                      <div class="hex-middle"></div>
                      <div class="hex-lower hex-box"></div>
                  </div>
                  <div class="full-square"></div>
                  <i class="sprite-Mail_icon"></i>
              </div>

              <div class="figcaption">
                  <h4>Taxi Service</h4>
                  <h5>There are many versions of lorem ipsum available</h5>
              </div>
          </article>
          
          <article class="col-md-3 col-sm-6 col-xs-6">
              <div class="hexagon figure">
                  <div class="full-hexagon">
                      <div class="hex-upper hex-box"></div>
                      <div class="hex-middle"></div>
                      <div class="hex-lower hex-box"></div>
                  </div>
                  <div class="full-square"></div>
                  <i class="sprite-Compass_icon"></i>
              </div>

              <div class="figcaption">
                  <h4>Delivery Service</h4>
                  <h5>There are many versions of lorem ipsum available</h5>
              </div>
          </article>
          
         
          
          <article class="col-md-3 col-sm-6 col-xs-6">
              <div class="hexagon figure">
                  <div class="full-hexagon">
                      <div class="hex-upper hex-box"></div>
                      <div class="hex-middle"></div>
                      <div class="hex-lower hex-box"></div>
                  </div>
                  <div class="full-square"></div>
                  <i class="sprite-Lupa_icon"></i>
              </div>
              <div class="figcaption">
                  <h4>24/7 Customer Support</h4>
                  <h5>There are many versions of lorem ipsum available</h5>
              </div>
          </article>
           <article class="col-md-3 col-sm-6 col-xs-6">
              <div class="hexagon figure">
                  <div class="full-hexagon">
                      <div class="hex-upper hex-box"></div>
                      <div class="hex-middle"></div>
                      <div class="hex-lower hex-box"></div>
                  </div>
                  <div class="full-square"></div>
                  <i class="sprite-Support_icon"></i>
              </div>

              <div class="figcaption">
                  <h4>Rent a car</h4>
                  <h5>There are many versions of lorem ipsum available</h5>
              </div>
          </article>
          </div>
      </div>
  </section>





<section id="some-fact" class="color_section parallax">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3 class="parallax-title text-center to_animate" data-animation="slideDown">Come Join Us As A Driver</h3>       
                <hr class="parallax-border to_animate" data-animation="fadeInUp"> 

            </div>
        </div>
        <div class="row">

        <div class="col-sm-6 col-md-3 to_animate" data-animation="fadeInUpBig">
          <div class="single_teaser icons style2">
            <div class="icons_introimg image-icon">
              <i class="fa fa-smile-o"></i>
            </div>
            <h3>Happy Clients</h3>
            <h2 class="counter" data-from="0" data-to="1350" data-speed="2500"></h2>          
          </div>
        </div>

        <div class="col-sm-6 col-md-3 to_animate" data-animation="fadeInUpBig">
          <div class="single_teaser icons style2">
            <div class="icons_introimg image-icon">
              <i class="fa fa-rocket"></i>
            </div>
            <h3>Careers Build</h3>
            <h2 class="counter" data-from="0" data-to="650" data-speed="3500"></h2>           
          </div>
        </div>

        <div class="col-sm-6 col-md-3 to_animate" data-animation="fadeInUpBig">

          <div class="single_teaser icons style2">
            <div class="icons_introimg image-icon">
              <i class="fa fa-trophy"></i>
            </div>
            <h3>Awards Won</h3>
            <h2 class="counter" data-from="0" data-to="780" data-speed="3500"></h2>          
          </div>
        </div>

        <div class="col-sm-6 col-md-3 to_animate" data-animation="fadeInUpBig">
          <div class="single_teaser icons style2">
            <div class="icons_introimg image-icon">
              <i class="fa fa-pencil"></i>
            </div>
            <h3>Briefs Wrote</h3>
            <h2 class="counter" data-from="0" data-to="1400" data-speed="2100"></h2>          
          </div>
        </div>
      </div>

    </div>
</section>



<section id="contact">
        <div class="container">
         <div class="row ">
            <div class="col-sm-6 col-sm-offset-3">   
             <h3 class="head-title text-center to_animate" data-animation="slideDown">Contact Us</h3>
              <p class="head-desc text-center to_animate" data-animation="fadeInUp">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                <hr class="title-border">
            </div>
         </div>

            <div class="row">
                <div class="block col-sm-6">
                    <h3>Drop us a message </h3>
                    <form class="contact-form" method="post" action="/">
                        <p class="contact-form-name">
                            <label for="name">Name <span class="required">*</span></label>
                            <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Name">
                        </p>
                        <p class="contact-form-email">
                            <label for="email">Email <span class="required">*</span></label>
                            <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email">
                        </p>
                        <p class="contact-form-subject">
                            <label for="subject">Subject <span class="required">*</span></label>
                            <input type="text" aria-required="true" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Subject">
                        </p>
                        <p class="contact-form-message">
                            <label for="message">Message</label>
                            <textarea aria-required="true" rows="8" cols="45" name="message" id="message" class="form-control" placeholder="Message"></textarea>
                        </p>
                        <p class="contact-form-submit vertical-margin-40">
                            <input type="submit" value="Send" id="contact_form_submit" name="contact_submit" class="btn btn-primary">
                        </p>
                    </form>
                </div>
                <div class="block widget_text col-sm-6">
                  <h3>Contact Info</h3>
                  <div id="map" class="light_section"></div>                    
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, aspernatur, velit. Adipisci, animi, molestiae, neque voluptatum non voluptas atque aperiam nam sed quidem quae harum ipsum sit veritatis expedita. Laudantium.</p>
                    <p>
                        <strong>Address:</strong> 8605 Santa Monica Blvd, LA, CA 97845, US<br> 
                        <strong>Phone: </strong>+28 356 859 4857<br>
                        <strong>Email: </strong><a href="mailto:info@company.com">info@company.net</a><br> 
                        <strong>Website: </strong><a href="./">www.company.net</a>
                    </p>
                    <h3>Find Us On</h3>
                    <p id="social-bottom">
                        <a class="socialico-facebook monochrome" href="#" title="Facebook">#</a>
                        <a class="socialico-twitter monochrome" href="#" title="Twitter">#</a>
                        <a class="socialico-google monochrome" href="#" title="Google">#</a>
                    </p>
                </div>
            </div>
        </div>
    </section>





    <section id="copyright" class="color_section">
        <div class="container">
            <div class="row">

                <div class="col-sm-12 text-center">
                    &copy; Copyright 2018-Norile
                </div>
            </div>

        </div>
    </section>
 

    <div class="preloader">
        <div class="preloaderimg"></div>
    </div>

        <script src="js/vendor/jquery-1.11.1.min.js"></script>
        <script src="js/vendor/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/placeholdem.min.js"></script>
        <script src="js/vendor/hoverIntent.js"></script>
        <script src="js/vendor/superfish.js"></script>
        <script src="js/vendor/jquery.actual.min.js"></script>
        <script src="js/vendor/jquery.appear.js"></script>
        <script src="js/vendor/jquerypp.custom.js"></script>
        <script src="js/vendor/jquery.elastislide.js"></script>
        <script src="js/vendor/jquery.flexslider-min.js"></script>
        <script src="js/vendor/jquery.prettyPhoto.js"></script>
        <script src="js/vendor/jquery.easing.1.3.js"></script>
        <script src="js/vendor/jquery.ui.totop.js"></script>
        <script src="js/vendor/jquery.isotope.min.js"></script>
        <script src="js/vendor/jquery.easypiechart.min.js"></script>
        <script src='js/vendor/jflickrfeed.min.js'></script>
        <script src="js/vendor/jquery.sticky.js"></script>
        <script src='js/vendor/owl.carousel.min.js'></script>
        <script src='js/vendor/jquery.nicescroll.min.js'></script>
        <script src='js/vendor/jquery.fractionslider.min.js'></script>
        <script src='js/vendor/jquery.scrollTo-min.js'></script>
        <script src='js/vendor/jquery.localscroll-min.js'></script>
        <script src='js/vendor/jquery.parallax-1.1.3.js'></script>
        <script src='js/vendor/jquery.bxslider.min.js'></script>
        <script src='js/vendor/jquery.funnyText.min.js'></script>
        <script src='js/vendor/jquery.countTo.js'></script>
        <script src="js/vendor/grid.js"></script>
        <script src='twitter/jquery.tweet.min.js'></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script type="text/javascript">$("#aboutbtn").click(function() {
    $('html, body').animate({
        scrollTop: $("#about").offset().top
    }, 2000);
});

</script>
   <script type="text/javascript">$("#joinbtn").click(function() {
    $('html, body').animate({
        scrollTop: $("#some-fact").offset().top
    }, 2000);
});

</script>
   <script type="text/javascript">$("#servicebtn").click(function() {
    $('html, body').animate({
        scrollTop: $("#offer").offset().top
    }, 2000);
});

</script>
   <script type="text/javascript">$("#contactbtn").click(function() {
    $('html, body').animate({
        scrollTop: $("#contact").offset().top
    }, 2000);
});

</script>
<script>
var video = document.getElementById("myVideo");
var btn = document.getElementById("myBtn");

function myFunction() {
  if (video.paused) {
    video.play();
    btn.innerHTML = "Pause";
  } else {
    video.pause();
    btn.innerHTML = "Play";
  }
}
</script>

<!-- Map Scripts -->

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript">

            var lat;
            var lng;
            var map;

            //type your address after "address="
            jQuery.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address=london, baker street, 221b&sensor=false', function(data) {
                lat = data.results[0].geometry.location.lat;
                lng = data.results[0].geometry.location.lng;
            }).complete(function(){
                dxmapLoadMap();
            });

            function attachSecretMessage(marker, message)
            {
                var infowindow = new google.maps.InfoWindow(
                    { content: message
                    });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);
                });
            }

            window.dxmapLoadMap = function()
            {
                var center = new google.maps.LatLng(lat, lng);
                var settings = {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoom: 16,
                    draggable: false,
                    scrollwheel: false,
                    center: center
                };
                map = new google.maps.Map(document.getElementById('map'), settings);

                var marker = new google.maps.Marker({
                    position: center,
                    title: 'Map title',
                    map: map
                });
                marker.setTitle('Map title'.toString());
            //type your map title and description here
            attachSecretMessage(marker, '<h3>Map title</h3>Map HTML description');
            }
        </script>




    </body>
</html>